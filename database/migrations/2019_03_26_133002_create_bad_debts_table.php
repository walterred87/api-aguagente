<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('bad_debts')) {
            return;
        }

        Schema::table('clients', function (Blueprint $table) {
            $table->addColumn('boolean', 'bad_debt');
        });

        Schema::create('bad_debts', function (Blueprint $table) {
            $table->increments('id_bad_debts');
            $table->char('id_charges', 32)->nullable(true);
            $table->unsignedInteger('id_clients')->nullable(false);
            $table->decimal('amount', 10, 2)->nullable(false);
            $table->decimal('collection_fees', 10, 2)->nullable(false);
            $table->decimal('moratory_fees', 10, 2)->nullable(false);
            $table->date('next_try_to_charge')->nullable(false);
            $table->longText('reason');
            $table->unsignedInteger('user_id')->nullable(false);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('clients')) {
            Schema::table('clients', function (Blueprint $table) {
                $table->dropColumn('bad_debt');
            });
        }
        Schema::dropIfExists('bad_debts');

    }
}
