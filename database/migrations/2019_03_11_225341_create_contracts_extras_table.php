<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('contracts_extras')) {
            return;
        }

        Schema::create('contracts_extras', function (Blueprint $table) {
            $table->increments('id_contracts_extras');
            $table->unsignedInteger('id_contracts')->nullable(false);
            $table->string('category_name', 255)->nullable(false);
            $table->string('element_name', 255)->nullable(false);
            $table->decimal('price', 10, 2)->nullable(false);
            $table->dateTime('paid_date')->default(NULL);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->foreign('id_contracts', 'contracts')
                ->references('id_contracts')
                ->on('contracts')
                ->onDelete('NO ACTION')
                ->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('contracts_extras')) {
            Schema::table('contracts_extras', function (Blueprint $table) {
                $table->dropForeign('contracts');
            });
            Schema::dropIfExists('contracts_extras');
        }
    }
}
