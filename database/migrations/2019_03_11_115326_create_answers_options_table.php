<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('answers_options')) {
            return;
        }
        Schema::create('answers_options', function (Blueprint $table) {
            $table->increments('id_answers_options');
            $table->unsignedInteger('id_answers')->nullable(false);
            $table->string('answer', 255);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->foreign('id_answers', 'answers_options_ibfk_1')
                ->references('id_answers')
                ->on('answers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('answers_options')) {
            Schema::table('answers_options', function (Blueprint $table) {
                $table->dropForeign('answers_options_ibfk_1');
            });
            Schema::drop('answers_options');
        }

    }
}
