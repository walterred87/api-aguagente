<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class UsersFilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereNotNull('image')->get();

        foreach ($users as $user) {
            if (strlen($user->image) > 20) {
                $imageName = $user->id . '-image-profile.' . str_replace("image/", "", $user->type);
                File::put(public_path() . '/profiles/' . $imageName, base64_decode($user->image));
                $user->image = $imageName;
                $user->save();
            }
        }
    }
}
