<?php

use Illuminate\Database\Seeder;
use App\NotificationType;
use App\Notification;

class NotificationTypesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NotificationType::create(
            array(
                'id_notification_types'   => NotificationType::PUSH,
                'name' => 'Notificación Push'
            )
        );

        NotificationType::create(
            array(
                'id_notification_types'   => NotificationType::EMAIL,
                'name' => 'Notificación por Correo'
            )
        );

        $notifications = Notification::all();

        foreach($notifications as $notification)
        {
            $notification->id_notification_type = $notification->type == Notification::PUSH ? NotificationType::PUSH : Notification::EMAIL;
            $notification->title = $notification->type == Notification::PUSH ? 'Notificación' : $notification->message;
            $notification->save();
        }
    }
}
