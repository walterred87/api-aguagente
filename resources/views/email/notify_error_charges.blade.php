@extends('layouts.email')

@section('name')
{{ $nombre }}
@endsection

@section('content')
@if($pay_tries < 4)
Tu tarjeta ha sido rechazada por el banco, por lo que No hemos podido realizar el cargo <strong>Aguagente: {{$periodo}}</strong>.<br>
Por favor comunícate con el banco para poder volver a solicitar el cargo y que se realice el pago.<br/>
Favor de enviar un correo a <a href="mailto:letty@aguagente.com">letty@aguagente.com</a> comunicando respuesta del banco.
@else
    Lamentablemente, aún no hemos podido cobrar su pago, según su contrato, la cláusula x, se cobrará un cargo por pago atrasado e intereses. Llámenos si necesita ayuda para realizar su pago antes de aplicar estas tarifas
@endif
<br/>

Gracias y disfruta de Aguagente!
@endsection