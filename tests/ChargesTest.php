<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ChargesTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testChargeIndex()
    {
        $this->get('/charges')
            ->assertResponseStatus(200);
    }

    public function testChargeShow()
    {
        $charge = $this->getObjectRandom(\App\Charge::class);
        $this->get('/charges/' . $charge->id_charges)
            ->assertResponseStatus(200);
    }

    public function testChargeCreate()
    {
        // issues conekta
        //$this->post('/charges/' . $charge->id_charges)
        //  ->assertResponseStatus(200);
    }

    public function testChargeRefund()
    {
        // conekta not found the id charge
        $charge = $this->getObjectRandom(\App\Charge::class);
        $this->post('/charges/' . $charge->id_charges . '/refund', ['amount' => 20])
            ->assertResponseStatus(200);
    }

}
