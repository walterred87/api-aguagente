<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RolesTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Role index test
     *
     * @return void
     */
    public function testRolesIndex()
    {
        $this->get('/roles')
            ->assertResponseStatus(200);
    }

    public function testRolesCreate()
    {
        $params = $this->getTestData('/data/roles/data.json');
        $this->post('/roles', $params)
            ->assertResponseStatus(201);
    }

    public function testRolesUpdate()
    {
        $params = $this->getTestData('/data/roles/data.json');
        $role = $this->getObjectRandom(\App\Role::class);
        $this->put('/roles/' . $role->id, $params)
            ->assertResponseStatus(200);
    }

    public function testRolesShow()
    {
        $role = $this->getObjectRandom(\App\Role::class);
        $this->get('/roles/' . $role->id)
            ->assertResponseStatus(200);
    }

    public function testRolesDestroy()
    {
        $role = $this->getObjectRandom(\App\Role::class);
        $this->delete('/roles/' . $role->id)
            ->assertResponseStatus(200);
    }

}
