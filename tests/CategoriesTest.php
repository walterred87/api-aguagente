<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoriesTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Contract index test
     *
     * @return void
     */
    public function testCategoryIndex()
    {
        $this->get('/categories')
            ->assertResponseStatus(200);
    }

    public function testCategoryCreate()
    {
        $params = $this->getTestData('/data/categories/create.json');
        $this->post('/categories', $params)
            ->assertResponseStatus(201);
    }

    public function testCategoryUpdate()
    {
        $params = $this->getTestData('/data/categories/create.json');
        $category = $this->getObjectRandom(\App\Category::class);
        $this->put('/categories/' . $category->id_categories, $params)
            ->assertResponseStatus(200);
    }

    public function testCategoryShow()
    {
        $category = $this->getObjectRandom(\App\Category::class);
        $this->get('/categories/' . $category->id_categories)
            ->assertResponseStatus(200);
    }

    public function testCategoryDestroy()
    {
        $category = $this->getObjectRandom(\App\Category::class);
        $this->delete('/categories/' . $category->id_categories)
            ->assertResponseStatus(200);
    }

}
