<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WithdrawalsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testWithdrawalIndex()
    {
        $this->get('/withdrawals')
            ->assertResponseStatus(200);
    }

    public function testWithdrawalShow()
    {
        $withdrawal = $this->getObjectRandom(\App\Withdrawal::class);
        $this->get('/withdrawals/' . $withdrawal->id_withdrawals)
            ->assertResponseStatus(200);
    }

    public function testWithdrawalCreate() {
        $params = [
            'amount' => 20,
            'date'   => date("Y-m-d"),
            'reason' => 'test reason'
        ];
        $this->post('/withdrawals',$params)
            ->assertResponseStatus(201);

    }

}
