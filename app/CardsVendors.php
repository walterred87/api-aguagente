<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardsVendors extends Model
{
    protected $table      = 'cards_vendors';
    protected $primaryKey = 'id_cards_vendors';

    public function card() {
        return $this->belongsTo('App\Card', 'id_cards');
    }

    public function vendor() {
        return $this->belongsTo('App\Vendors', 'id_vendor');
    }
}
