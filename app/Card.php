<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table      = 'cards';
    protected $primaryKey = 'id_cards';

    public function client() {
        return $this->belongsTo('App\Client', 'id_clients');
    }

    public function card_vendor() {
        return $this->hasMany('App\Cards_Vendors', 'id_cards');
    }

}
