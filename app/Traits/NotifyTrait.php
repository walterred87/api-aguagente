<?php

namespace App\Traits;

use App\Debt;
use App\Contract;
use App\Client;
use App\Charge;
use Request;
use Mail;
use App\Notification;
use App\NotificationType;

trait NotifyTrait
{
    public $production = true;


    public function __construct()
    {
        //$this->production = (env('ENVIRONMENT_EMAIL') ==  'production');
    }

    /**
     * $staff
     *
     * @var array
     */
    protected $staff = [
        'servicio@aguagente.com',
        'raiders11@gmail.com'
    ];

    /**
     * $admins
     *
     * @var array
     */
    protected $admins = [
        'it@aguagente.com'
    ];

    /**
     * $contacts
     *
     * @var array
     */
    protected $contacts = [
        'contacto@aguagente.com'
    ];

    /**
     * $devs
     *
     * @var array
     */
    protected $devs = [
        //'raiders11@gmail.com',
        'it@aguagente.com',
        //'garry@aguagente.com'
    ];

    protected $cco = [
        'letty@aguagente.com'
    ];

    /**
     * $statusContract
     *
     * @var string
     */
    protected $statusContract = "Estatus de contrato";

    /**
     * $ticketCreation
     *
     * @var string
     */
    protected $ticketCreation = "Creación de ticket # ";

    /**
     * $ticketFinished
     *
     * @var string
     */
    protected $ticketFinished = "Finalización de ticket # ";

    /**
     * $ticketAssigned
     *
     * @var string
     */
    protected $ticketAssigned = "Asignación de ticket # ";

    /**
     * notifyUserOffline
     * Envía un email con la referencia generada para su pago OFFLINE.
     * 
     * @param string $type  Tipo de notificación(OXXO/SPEI)
     * @param object $order Objeto de CONEKTA con la respuesta del cargo.
     * @param Client $client
     * @return void
     */
    public function notifyUserOffline($type, $order, $client)
    {
        $view = 'email.oxxo';
        $data = [
            'reference' => $order->charges[0]->payment_method->reference,
            'total'     => $order->amount / 100,
            'nombre'    => $client->name
        ];

        if ($type == 'SPEI') {
            $view        = 'email.spei';
            $data['reference'] = $order->charges[0]->payment_method->clabe;
            $data['bank']      = $order->charges[0]->payment_method->bank;
        }

        $to  = $this->devs;
        $cco = $this->devs;
        if ($this->production) {
            $to  = $client->email;
            $cco = $this->cco;
        }

        Mail::send($view, $data, function ($message) use ($client, $to, $cco) {
            $message->subject('Referencia de pago');
            $message->to($to);
            $message->bcc($cco);
            $message->replyTo('contacto@aguagente.com', 'Contacto');
        });
    }

    /**
     * notifyStaffOffilinePayment
     * Se le envía un email al staff cuando un pago offline se realice exitosamente. 
     * 
     * @param array $params Array de parametros
     * @return void
     */
    public function notifyStaffOffilinePayment($params)
    {
        $to = $this->devs;
        if ($this->production) {
            $to = $this->staff;
        }

        Mail::send('email.offline_payment', $params, function ($message) use ($to) {
            $message->subject('Pago offline exitoso');
            $message->replyTo('contacto@aguagente.com', 'Contacto');
            $message->to($to);
        });
    }

    /**
     * notifyPassword
     * Se le envía el email de bienvenida al cliente con su password.
     *
     * @param array $params Array de parametros
     * @return void
     */
    public function notifyPassword($params)
    {
        $test = [
            'isProduction' => $this->production,
            'devs'   => $this->devs
        ];
        Mail::send(
            'email.passwordNotification',
            array(
                'name'     => $params['client']->name,
                'username' => $params['client']->email,
                'password' => $params['password']
            ),
            function ($message) use ($params, $test) {
                $message
                    ->replyTo('contacto@aguagente.com', 'Contacto')
                    ->subject('Bienvenido a Aguagente');

                if ($test['isProduction']) {
                    $message->to($params['client']->email, $params['client']->name);
                } else {
                    $message->to($test['devs']);
                }
            }
        );

        $to = $this->devs;
        if ($this->production) {
            $to = $this->staff;
        }
        Mail::send(
            'email.staff_new_user',
            array(
                'client' => $params['client']
            ),
            function ($message) use ($params, $to) {
                $message
                    ->to($to)
                    ->replyTo('contacto@aguagente.com', 'Contacto')
                    ->subject('Nuevo registro');
            }
        );
    }

    public function notifyAdmin($msg = '')
    {
        $to = $this->devs;
        if ($this->production) {
            $to = $this->admins;
        }

        Mail::send(
            'email.staff_error',
            [
                'msg' => $msg
            ],
            function ($message) use ($params, $to) {
                $message
                    ->to($to)
                    ->replyTo('contacto@aguagente.com', 'Contacto')
                    ->subject('Error procesando pago CRONJOB');
            }
        );
    }

    public function notifyStaff($client)
    {
        $data = array(
            'name' => 'Staff Aguagente',
            'msg'  => 'Se actualizo el contrato de ' . $client->name
        );
        $this->sendEmail('email.generic', $data, $this->staff, 'Staff Aguagente', 'Contrato Actualizado');
    }

    public function notifyStatusContract($client, $status, $reasons = null)
    {
        switch ($status) {
            case 'accepted':
                $data = array(
                    'name'    => $client->name,
                    'status'  => 'Aceptado',
                    'reasons' => 'Aceptado'
                );
                break;

            case 'invalid':
                $data = array(
                    'name'    => $client->name,
                    'status'  => 'Invalidado',
                    'reasons' => $reasons
                );
                break;

            case 'rejected':
                $data = array(
                    'name'    => $client->name,
                    'status'  => 'Rechazado',
                    'reasons' => $reasons
                );
                break;

            case 'canceled':
                $data = array(
                    'name'    => $client->name,
                    'status'  => 'Cancelado',
                    'reasons' => $reasons
                );
                break;
        }

        $this->sendEmail('email.status', $data, $client->email, $client->name, $this->statusContract, $reasons, $client->id_users);
    }

    public function ticketCreation($ticket)
    {
        $data = array('ticket' => $ticket);
        $this->sendEmail(
            'ticket.creation',
            $data,
            $ticket->client->email,
            $ticket->client->name,
            $this->ticketCreation . $ticket->id_tickets,
            $this->ticketCreation . $ticket->id_tickets,
            $ticket->client->id_users
        );
    }

    public function completionTicket($ticket, $completion, $technician)
    {
        $data = array(
            'technician' => $technician,
            'ticket'     => $ticket,
            'completion' => $completion
        );
        $this->sendEmail(
            'ticket.completion',
            $data,
            $ticket->client->email,
            $ticket->client->name,
            $this->ticketFinished . $ticket->id_tickets,
            $this->ticketFinished . $ticket->id_tickets,
            $ticket->client->id_users
        );
    }

    public function assignedTicket($ticket, $assignation, $technician)
    {

        $data = array(
            'technician'  => $technician,
            'ticket'      => $ticket,
            'assignation' => $assignation
        );

        $this->sendEmail(
            'ticket.assignation',
            $data,
            $technician->email,
            $technician->name,
            $this->ticketAssigned . $ticket->id_tickets,
            $this->ticketAssigned . $ticket->id_tickets,
            $ticket->client->id_users
        );

        $data = array(
            'technician' => $technician,
            'ticket'     => $ticket
        );

        $this->sendEmail(
            'ticket.assignation-client',
            $data,
            $ticket->client->email,
            $ticket->client->name,
            $this->ticketAssigned . $ticket->id_tickets,
            $this->ticketAssigned . $ticket->id_tickets,
            $ticket->client->id_users
        );
    }


    public function endService($ticket, $completion, $technician)
    {
        $data = array(
            'technician' => $technician,
            'ticket'     => $ticket,
            'completion' => $completion
        );

        $this->sendEmail(
            'ticket.completion',
            $data,
            $ticket->client->email,
            $ticket->client->name,
            $this->ticketFinished . $ticket->id_tickets,
            $this->ticketFinished . $ticket->id_tickets,
            $ticket->client->id_users
        );
    }

    public function contactEmail($client)
    {

        $data = array(
            'nombre'   => $client['nombre'],
            'correo'   => $client['correo'],
            'telefono' => $client['telefono'],
            'interesa' => $client['interesa'],
            'mensaje'  => $client['mensaje']
        );

        $this->sendEmail(
            'email.contact',
            $data,
            $this->contacts,
            'Contacto',
            'Contacto'
        );
    }

    public function sentPassword($input)
    {

        $data = array(
            'name'     => $input['name'],
            'username' => $input['email'],
            'password' => $input['password']
        );

        $this->sendEmail(
            'email.passwordNotification',
            $data,
            $input['email'],
            $input['name'],
            'Bienvenido a Aguagente'
        );
    }

    public function sentConfirmationEmail($input)
    {
        $data = array(
            'nombre'       => $input['name'],
            'confirm_data' => 'https://panelaguagente.xyz/api/public/auth/facebook-register?seller='.$input['seller'].'&manual_register=false&social_login=true&decode=true&params=' . base64_encode(json_encode($input))
        );

        $this->sendEmail(
            'email.confirm',
            $data,
            $input['email'],
            $input['name'],
            'Bienvenido a Aguagente'
        );
    }

    public function sendInvoiceToClient($invoice, $messageInvoice)
    {
        $name    = $invoice->client->name;
        $email   = $invoice->client->email;
        $subject = "Envio de Factura";
        $data    = array(
            'name'            => $name,
            'date'            => $invoice->date_stamp,
            'message_invoice' => $messageInvoice
        );
        $directory = public_path('invoices_files/');
        $pdfFile   = $directory . $invoice->filename . '.pdf';
        $xmlFile   = $directory . $invoice->filename . '.xml';

        $test = [
            'isProduction' => $this->production,
            'devs'   => $this->devs
        ];

        Mail::send(
            'email.send_invoice',
            $data,
            function ($message) use ($email, $name, $subject, $pdfFile, $xmlFile, $test) {

                $message->attach($pdfFile);
                $message->attach($xmlFile);
                $message->subject($subject);

                if ($test['isProduction']) {
                    $message->to($email, $name);
                } else {
                    $message->to($test['devs']);
                }
            }
        );
        Notification::create(array(
            'id_users'              => $invoice->client->id_users,
            'message'               => $subject,
            'type'                  => NotificationType::EMAIL,
            'title'                 => $subject,
            'id_notification_types' => NotificationType::EMAIL
        ));
    }

    public function notifyClientErrorCharges($client, $periodo)
    {
        $data = array(
            'nombre'    => $client->name,
            'pay_tries' => $client->pay_tries,
            'periodo'   => $periodo
        );


        $this->sendEmail(
            'email.notify_error_charges',
            $data,
            $client->email,
            $client->name,
            'Error de cargo',
            "Intento de cargo #$client->pay_tries",
            $client->id_users
        );
    }

    /**
     * Notify a client that his card is close to expire
     * @param Client $client
     *
     */
    public function notifyClientExpiringCard($client)
    {
        $data = array(
            'nombre' => $client->name
        );

        $this->sendEmail(
            'email.notify_expiring_card',
            $data,
            $client->email,
            $client->name,
            'Tarjeta próxima a expirar',
            'Tarjeta próxima a expirar',
            $client->id_users
        );
    }

    public function notifyClientSuccessCharges($client, $periodo)
    {
        $data = array(
            'nombre'    => $client->name,
            'pay_tries' => $client->pay_tries,
            'periodo'   => $periodo
        );

        $this->sendEmail(
            'email.notify_success_charges',
            $data,
            $client->email,
            $client->name,
            'Cargo Exitoso',
            'Cargo Exitoso',
            $client->id_users
        );
    }

    private function sendEmail($view, $data, $email, $name, $subject, $messageInfo = null, $id_users = null)
    {
        $test = [
            'isProduction' => $this->production,
            'devs'   => $this->devs
        ];

        // $email = 'gus.knul@gmail.com';
        Mail::send(
            $view,
            $data,
            function ($message) use ($email, $name, $subject, $test, $view) {
                $message->replyTo($view == 'ticket.completion' ? 'no-reply@aguagente.com' : 'contacto@aguagente.com', 'Contacto')
                    ->subject($subject);

                if ($test['isProduction']) {
                    $message->to($email, $name);
                } else {
                    $message->to($test['devs']);
                }
            }
        );

        if ($id_users != null) {
            //dd($data, $email, $name, $subject, $message);
            Notification::create([
                'id_users'              => $id_users,
                'message'               => $messageInfo ? $messageInfo : $subject,
                'id_notification_types' => NotificationType::EMAIL,
                'title'                 => $subject,
                'type'                  => NotificationType::EMAIL,
            ]);
        }
    }

    public function sendNotifyHck()
    {
        Mail::send(
            'email.notify_hack',
            [
                'name' => 'Letty Zambrano'
            ],
            function ($message) {
                $message->subject('Notificación Importante (Español) / Important Notification (Scroll down for English)');
                //$message->to('it@aguagente.com', 'Backups');
                $message->to('letty@aguagente.com', 'Letty Zambrano');
            }
        );
    }

    public function sendDump($date, $file)
    {
        Mail::send(
            'email.dump',
            [],
            function ($message) use ($date, $file) {
                $message->subject('Dump '.$date);
                $message->to('it@aguagente.com', 'Backups');
                // $message->to('raiders11@gmail.com', 'Backups');
                $message->attach($file);
            }
        );
    }

    public function nofifyStaffHubspotError($client, $data)
    {
        $data = [
            'name' => 'Staff Aguagente',
            'msg'  => 'Sucedio un error registrando el cliente en hubspot con los siguientes datos: ' .
                "Email: {$client->email}, Nombre: {$client->name}, Teléfono: {$client->phone}, Favor de
                intentarlo manualmente. Errors: " . json_encode($data)
        ];
        $this->sendEmail('email.generic', $data, $this->staff, 'Staff Aguagente', 'Registro Contacto Hubspot fallido');
    }
}
