<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationType extends Model
{
    protected $table = 'notification_types';
    protected $primaryKey = 'id_notification_types';
    protected $fillable = ['name'];

    const PUSH = 1;
    const EMAIL = 2;


    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }
}
