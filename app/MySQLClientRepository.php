<?php namespace App;

use App\Client;
use App\Level;
use App\IClientRepository;
use Illuminate\Support\Facades\DB;

class MySQLClientRepository implements IClientRepository {

	/**
	 * getById
	 * Obtiene todos los clientes por medio de su ID
	 * @param  array $id_clients
	 * @return Client[]             Array de Eloquent Client
	 */
	public function getById($id_clients) {

		return Client::find($id_clients);

	}

	/**
	 * getPayingClients
	 * Obtiene todos los clientes que sean referidos por otro cliente($id_clients), que no 
	 *    tengan nivel "VIAJERO", que esten activos y no tengan deuda.
	 * @param  array $id_clients 
	 * @return Client[]             Array de Eloquent Client
	 */
	public function getPayingClients($id_clients) {

		return DB::table('clients')
		    ->whereRaw("clients.referred_by = ? && ((clients.level != 'VIAJERO') OR ((clients.status = 'accepted') && (clients.debt = 0)))", array($id_clients))
		    ->get();

	}

	/**
	 * getSignedClientsTree
	 * Obtiene el ARBOL de los niveles con los clientes de cada nivel.
	 * 
	 * @param  array $id_clients 
	 * @return array             Array de niveles, con los clientes de cada nivel
	 */
	public function getSignedClientsTree($id_clients) {

		$clients = DB::table('clients AS t0')
			->select(
				't1.id_clients AS level_1',
				't2.id_clients AS level_2',
				't3.id_clients AS level_3',
				't4.id_clients AS level_4'
			)
			->leftJoin('clients as t1', 't1.referred_by', '=', 't0.id_clients')
			->leftJoin('clients as t2', 't2.referred_by', '=', 't1.id_clients')
			->leftJoin('clients as t3', 't3.referred_by', '=', 't2.id_clients')
			->leftJoin('clients as t4', 't4.referred_by', '=', 't3.id_clients')
			->where('t0.id_clients', '=', $id_clients)
			->get();

		$clients = array_map(
			function($client) {
				return (array)$client;
			},
			$clients
		);

		$level_1 = array_filter(array_column($clients, 'level_1'), function($value) { return $value != null; });
		$level_2 = array_filter(array_column($clients, 'level_2'), function($value) { return $value != null; });
		$level_3 = array_filter(array_column($clients, 'level_3'), function($value) { return $value != null; });
		$level_4 = array_filter(array_column($clients, 'level_4'), function($value) { return $value != null; });

		return [
			'level_1' => count(array_unique($level_1)),
			'level_2' => count(array_unique($level_2)),
			'level_3' => count(array_unique($level_3)),
			'level_4' => count(array_unique($level_4))
		];

	}

	/**
	 * getSignedClientsTree2
	 * Obtiene todos los clientes, aunque estén repetidos.
	 * 
	 * @param  array $id_clients 
	 * @return array             Array de niveles, con los clientes de cada nivel
	 */
	public function getSignedClientsTree2($id_clients) {

		$clients = DB::table('clients AS t0')
			->select(
				't1.id_clients AS level_1',
				't1.debt AS debt_1',
				't2.id_clients AS level_2',
				't2.debt AS debt_2',
				't3.id_clients AS level_3',
				't3.debt AS debt_3',
				't4.id_clients AS level_4',
				't4.debt AS debt_4'
			)
			->leftJoin('clients as t1', 't1.referred_by', '=', 't0.id_clients')
			->leftJoin('clients as t2', 't2.referred_by', '=', 't1.id_clients')
			->leftJoin('clients as t3', 't3.referred_by', '=', 't2.id_clients')
			->leftJoin('clients as t4', 't4.referred_by', '=', 't3.id_clients')
			->where('t0.id_clients', '=', $id_clients)
			->get();

		/*if ($withNoDebt) {
			$clients->where('t1.debt', '=', '0.00');
			$clients->where('t2.debt', '=', '0.00');
			$clients->where('t3.debt', '=', '0.00');
			$clients->where('t4.debt', '=', '0.00');
		}*/

		//$clients->get();

		return $levels = array_map(
			function($client) {
				return (array)$client;
			},
			$clients
		);

		/*$root = Client::find($id_clients)->toArray();

        switch($root['level']) {

            case Level::VIAJERO: case Level::MARINERO:
                $levels = [
                    'level_1' => array_column($clients, 'level_1')
                ];
                break;
            case Level::CABO:
                $levels = [
                    'level_1' => array_column($clients, 'level_1'),
                    'level_2' => array_column($clients, 'level_2')
                ];
                break;
            case Level::CAPITAN:
                $levels = [
                    'level_1' => array_column($clients, 'level_1'),
                    'level_2' => array_column($clients, 'level_2'),
                    'level_3' => array_column($clients, 'level_3')
                ];
                break;
            case Level::ALMIRANTE:
                $levels = [
                    'level_1' => array_column($clients, 'level_1'),
                    'level_2' => array_column($clients, 'level_2'),
                    'level_3' => array_column($clients, 'level_3'),
                    'level_4' => array_column($clients, 'level_4')
                ];
                break;

        }

        $tree[] = $root;

        foreach ($levels['level_1'] as $key => $client) {

            if( $client!== null && !isset($tree[0]['clients'][$client])) {

                $tree[0]
                    ['clients'][$client] =
                        Client::find($client)->toArray();

            }

            if(isset($levels['level_2'])) {

                if($levels['level_2'][$key] !== null && !isset($tree[0]['clients'][$client]['clients'][$levels['level_2'][$key]])) {

                    $tree[0]
                        ['clients'][$client]
                            ['clients'][$levels['level_2'][$key]]
                                = Client::find($levels['level_2'][$key])->toArray();



                }

            }
            if(isset($levels['level_3'])) {

                if($levels['level_3'][$key] !== null && !isset($tree[0]['clients'][$client]['clients'][$levels['level_2'][$key]]['clients'][$levels['level_3'][$key]] )) {

                    $tree[0]
                        ['clients'][$client]
                            ['clients'][$levels['level_2'][$key]]
                                ['clients'][$levels['level_3'][$key]]
                                    = Client::find($levels['level_3'][$key])->toArray();

                }

            }
            if(isset($levels['level_4'])) {

                if($levels['level_4'][$key] !== null && !isset($tree[0]['clients'][$client]['clients'][$levels['level_2'][$key]]['clients'][$levels['level_3'][$key]]['clients'][$levels['level_4'][$key]] )) {

                    $tree[0]
                        ['clients'][$client]
                            ['clients'][$levels['level_2'][$key]]
                                ['clients'][$levels['level_3'][$key]]
                                    ['clients'][$levels['level_4'][$key]]
                                        = Client::find($levels['level_4'][$key])->toArray();

                }

            }

        }

        return $tree;*/

	}

	/**
	 * getSignedClientsFromThePastMonths
	 * Obtiene todos los clientes referidos por un cliente($id_clients), registrados en cierto rango de meses
	 * 
	 * @param  array   $id_clients 
	 * @param  integer $months     
	 * @return Client[]             Array de Clients Eloquent
	 */
	public function getSignedClientsFromThePastMonths($id_clients, $months) {

		return DB::table('clients')
			->select('*')
			->where('referred_by', '=', $id_clients)
			->whereRaw(
				"created_at >= CURDATE() - INTERVAL $months MONTH"
			)
			->get();

	}

	/**
	 * getCommissionsFromPastMonth
	 * Obtiene las suma comisiones pagadas en el ultimo mes.
	 * 
	 * @param  array $id_clients 
	 * @return float
	 */
	public function getCommissionsFromPastMonth($id_clients) {

		return DB::table('commissions')
			->where('id_clients', '=', $id_clients)
			->whereRaw(
				"YEAR(paid_at) =  YEAR(CURDATE() - INTERVAL 1 MONTH) AND
				MONTH(paid_at) = MONTH(CURDATE() - INTERVAL 1 MONTH)"
			)
			->sum('amount');

	}

	/**
	 * save
	 * Guarda el Eloquent Client objeto.
	 * 
	 * @param  Client $client 
	 * @return void
	 */
	public function save(Client $client) {

		$client->save();

	}

}
