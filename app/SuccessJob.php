<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuccessJob extends Model
{
    protected $table ='success_jobs';

    protected $fillable = ['payload'];
}
