<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Client;
use App\Ticket;

class CreateFilterChangeTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clients:filter-change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates the filter change ticket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Obtiene todos los Sales agents y calcula las comisiones del mes en curso.
     *
     * @return void
     */
    public function handle()
    {
        $today = Carbon::now()->endOfMonth();
        $service_time;
        $clients = Client::whereRaw('MONTH(created_at) = ' . $today->month)->where('status', 'accepted')->get();
        // $code = 1;
        $errorCode = [
            1 => 69, //odd
            2 => 70,  //even
            3 => 71
        ];

        $this->info('Fecha: '.$today->year.'-'.$today->month.'-'.$today->day);
        foreach ($clients as $client) {
            $diff = $today->diffInYears($client->created_at);
            if ($diff % 2 != 0) {
                if ($diff % 1 == 0) {
                    $code = 1;
                    $service_time = '1 año';
                }
            } else {
                if ($diff % 4 == 0) {
                    $code = 3;
                    $service_time = '4 años';
                } else {
                    $code = 2;
                    $service_time = '2 años';
                }
            }


            $ticket = new Ticket;
            $ticket->id_clients                    = $client->id_clients;
            $ticket->id_error_codes                = $errorCode[$code];
            $ticket->description                   = 'Cambio anual de filtros (' . ($service_time) . ')';
            $ticket->estimated_service_fee         = null;
            $ticket->estimated_service_fee_reasons = null;
            $ticket->status                        = 'opened';
            $ticket->type                          = 'filter_change';
            $ticket->save();

            $this->info('Cliente: ' . $client->name . ' | Ticket: ' . $ticket->description);
        }
        $this->info('-----------------------------------------------');
    }
}
