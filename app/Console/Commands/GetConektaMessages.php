<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Charge;
use Conekta;
use Conekta_Customer;
use Conekta_Charge;
use Conekta_Event;
use Conekta_Plan;
use Conekta_ResourceNotFoundError;
use Conekta_ProcessingError;
use Conekta_Handler;

class GetConektaMessages extends Command
{
    protected $signature = 'charges:get-error-messages';

    protected $description = 'Job to get the error messages from charges in Conekta';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('     ');
        $this->info('Fecha: '.date('Y-m-d'));
        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
        Conekta::setLocale('es');
        
        $updated = [];
        $charges = Charge::whereNull('paid_at')->where('failure_message', '')->orderBy('created_at', 'DESC')->get();
        foreach($charges as $charge) {
            $params = ['id' => $charge->id_charges];
            $conektaCharge = json_decode(Conekta_Charge::where($params)->__toJSON())[0];
            
            $charge->failure_message = $conektaCharge->failure_message;
            $updated[] = $charge;

            $this->info('Charge: '.$charge->id_charges.' | Message: '.$conektaCharge->failure_message);

            $charge->save();
        }
    }
}
