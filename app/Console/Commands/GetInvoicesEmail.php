<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Webklex\IMAP\Client as ClientImap;
use Illuminate\Support\Facades\Storage;
use App\Invoice;
use App\Client;

class GetInvoicesEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:get-from-email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all invoices in  Imap Account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oClient = \Webklex\IMAP\Facades\Client::account('default');
        $oClient->connect();

        $aFolder = $oClient->getFolders();

        foreach ($aFolder as $oFolder) {
            $aMessage = $oFolder->query()->unseen()->get();

            foreach ($aMessage as $oMessage) {
                $aAttachments = $oMessage->getAttachments();
                $aAttachments->each(function ($oAttachment) {
                    $typeDocument = $oAttachment->getExtension();

                    if ($typeDocument == 'xml' || $typeDocument == 'pdf') {
                        Storage::disk('invoices')->put($oAttachment->getName(), $oAttachment->getContent());

                        if ($typeDocument == 'xml') {
                            $xmlPath   = public_path('invoices_files/' . $oAttachment->getName());
                            $doc       = new \DOMDocument;
                            $xmlReader = new \XMLReader;
                            $xmlReader->open($xmlPath);
                            $attachmentStringXML = $oAttachment->getContent();
                            $dateStamp           = null;

                            while ($xmlReader->read()) {
                                if ($xmlReader->name == 'cfdi:Comprobante')
                                {
                                    $nodeInvoice  = simplexml_import_dom($doc->importNode($xmlReader->expand(), true));
                                    $arrayInvoice = $this->getAssociativeArrayXml($nodeInvoice->attributes());
                                    $dateStamp    = $arrayInvoice["Fecha"];
                                }

                                if ($xmlReader->name == 'cfdi:Receptor') 
                                {
                                    $nodeReceptor  = simplexml_import_dom($doc->importNode($xmlReader->expand(), true));
                                    $arrayReceptor = $this->getAssociativeArrayXml($nodeReceptor->attributes());
                                    $rfc    = $arrayReceptor["Rfc"];
                                    $client = Client::where('rfc', $rfc)->orWhere('invoice_data', 'like', "%$rfc%")->first();

                                    if ($client != null)
                                    {
                                        Invoice::create(
                                            array(
                                                'xml'        => $attachmentStringXML,
                                                'id_clients' => $client->id_clients,
                                                'date_stamp' => $dateStamp,
                                                'filename'   => str_replace('.xml', '', $oAttachment->getName())
                                            )
                                        );
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
    }

    private function getAssociativeArrayXml($attributes)
    {
        $arrayData = [];
        foreach ($attributes as $a => $b) {
            $arrayData[$a] = (String)$b;
        }
        return $arrayData;
    }
}
