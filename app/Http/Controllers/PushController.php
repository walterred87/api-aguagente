<?php

namespace App\Http\Controllers;

use App\Device;
use App\Jobs\SendNotificationEmail;
use App\Notification;
use App\User;
use Davibennun\LaravelPushNotification\PushNotification;
use Request;
use DB;
use Validator;
use App\NotificationGroup;
use App\NotificationType;

class PushController extends Main
{

    /**
     * push
     * Envía una notificación "Push" a los dispositivos registrados.
     *
     * @Davibennun\LaravelPushNotification\PushNotification;
     * @App\Notification;
     *
     * @param  array $devices Objeto Eloquent de Dispositivos.
     * @param  string $message Mensaje de notificacion
     * @return mixed          void|ERROR
     */
    public function push($users, $message)
    {
        $idUsers = [];
        $names = [];
        foreach ($users as $user) {
            array_push($idUsers, $user->id);
            array_push($names, $user->name);
        }

        $devices = Device::whereIn('id_users', $idUsers)->get();
        $subject = 'Notificación Automatica';
        foreach ($users as $user) {

            Notification::create(array(
                'id_users'             => $user->id,
                'message'              => $message,
                'type'                 => NotificationType::EMAIL,
                'id_notification_type' => NotificationType::EMAIL,
                'title'                => $subject
            ));

            $this->dispatch((new SendNotificationEmail($user, $message)));
        }

        /*
        $push = new PushNotification();

        foreach($devices as $device) {

            $devices[$device->platform][] = $push->Device($device->registration_id, array('badge' => 1));
            $devices['users'][] = $device->id_users;

        }

        try {

            if(isset($devices['ios']))
                $push
                    ->app('aguagente-push-ios')
                    ->to($push->DeviceCollection($devices['ios']))
                    ->send($message);

            if(isset($devices['android']))
                $push
                    ->app('aguagente-push-android')
                    ->to($push->DeviceCollection($devices['android']))
                    ->send($message);

            foreach(array_unique($devices['users']) as $id_users) {

                $notification = new Notification;
                $notification->id_users = $id_users;
                $notification->message = $message;
                $notification->save();

            }

        } catch(\Exception $e) {

            file_put_contents('push', $e->getMessage());

        }
        */

    }

    /**
     * notifyUser
     * Funcione principal que inicia el proceso para enviar notificaciones "PUSH" a clientes.
     *  Obtiene el array de clientes y el mensaje por medio de la variable GET.
     *
     * @Illuminate\Foundation\Http\FormRequest
     *
     * @return response OK|Bad Request
     */
    public function notifyUser()
    {

        $input = Request::all();

        $rules = [
            'users' => 'required|array',
            'message' => 'required|string'
        ];

        if (isset($input['users']))
            foreach ($input['users'] as $id_users)
                $rules['users.0'] = 'exists:users,id';

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

        $this->push(
            User::whereIn('id', $input['users'])->get(),
            $input['message']
        );

        return Main::response(true, 'OK', null, 200);

    }

    /**
     * notifyGroup
     * Funcion principal que inicia el proceso para enviar notificaciones "PUSH" a un grupo de clientes.
     *  Obtiene el array de clientes y el mensaje por medio de la variable GET.
     *
     * @Illuminate\Foundation\Http\FormRequest
     *
     * @return response OK|Bad Request
     */
    public function notifyGroup()
    {

        $input = Request::all();

        $rules = [
            'groups' => 'required|array',
            'message' => 'required|string'
        ];

        if (isset($input['groups']))
            foreach ($input['groups'] as $id_groups)
                $rules['groups.0'] = 'exists:groups,id_groups';

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

        
        $users = DB::table('users')->join('clients', 'users.id', '=', 'clients.id_users')->whereIn('clients.id_groups', $input['groups'])->get();
        $this->push(
            $users,
            $input['message']
        );
        return Main::response(true, 'OK', null, 200);

    }

    /**
     * notifyRole
     * Funcion principal que inicia el proceso para enviar notificaciones "PUSH" a los usuarios que tengan cierto rol.
     *  Obtiene el array de roles y el mensaje por medio de la variable GET.
     *
     * @Illuminate\Foundation\Http\FormRequest
     *
     * @return response OK|Bad Request
     */
    public function notifyRole()
    {

        $input = Request::all();

        $rules = [
            'roles' => 'required|array',
            'message' => 'required|string'
        ];

        if (isset($input['roles']))
            foreach ($input['roles'] as $id_roles)
                $rules['roles.0'] = 'exists:roles,id';

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

        $users = DB::table('users')->join('role_user', 'role_user.user_id', '=', 'users.id')->whereIn('role_user.role_id', $input['roles'])->get();
        $this->push(
            $users,
            $input['message']
        );

        return Main::response(true, 'OK', null, 200);

    }

    /**
     * notifyUsersGroup
     * Funcion principal que inicia el proceso para enviar notificaciones "PUSH" a un grupo de usuartos seleccionados.
     *
     * @Illuminate\Foundation\Http\FormRequest
     *
     * @return response OK|Bad Request
     */

     public function notifyUsersGroup()
   {
    $input = Request::all();

    $rules = [
        'id_notification_groups' => 'required',
        'message' => 'required|string'
    ];
    
    $validator = Validator::make($input, $rules);

    if ($validator->fails()) {

        return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

    }
    
    $notificationGroups = NotificationGroup::find($input['id_notification_groups']);
    $message = $input['message'];
    $users = $notificationGroups->users;
    $this->push(
        $users,
        $message
    );

    return Main::response(true, 'OK', null, 200);
   }  

}
