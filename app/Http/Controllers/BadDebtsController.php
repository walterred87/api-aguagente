<?php

namespace App\Http\Controllers;

use App\BadDebt;
use App\Client;
use App\Debt;
use Illuminate\Support\Facades\Auth;
use Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BadDebtsController extends Main
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $debts = Debt::all();
        return Main::response(true, 'OK', $debts, 200);
    }

    /**
     * Move the client to status bad_debt and the data in the debts is moved at the table bad debts if the client
     * has a bad debt
     *
     * @param int $id_clients ID del cliente
     *
     * @return response OK|Internal Server Error
     */
    public function moveBadDebt($id_clients)
    {

        $input = Request::all();

        $validator = Validator::make($input, ['reason' => 'required']);

        if ($validator->fails()) {
            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        }

        $client = Client::where('id_clients', $id_clients)->first();
        if (!$client) {
            return Main::response(false, null, null, 404);
        }
        $debts = Debt::where('id_clients', $client->id_clients)
            ->whereNull('id_charges')->get();

        if (count($debts) == 0) {
            return Main::response(false, 'Not Found', null, 200);
        }

        $bad_debts = [];
        foreach ($debts as $debt) {
            $badDebt = new BadDebt;
            $badDebt->id_clients = $debt->id_clients;
            $badDebt->id_charges = $debt->id_charges;
            $badDebt->amount = $debt->amount;
            $badDebt->collection_fees = $debt->collection_fees;
            $badDebt->moratory_fees = $debt->moratory_fees;
            $badDebt->next_try_to_charge = $debt->next_try_to_charge;
            $badDebt->reason = $input['reason'];
            $user = Auth::user();
            $badDebt->user_id = $user->id;
            $badDebt->save();
            $debt->delete();
            $bad_debts[] = $badDebt;
        }

        $client->bad_debt = true;
        $client->save();

        return Main::response(true, 'OK', $bad_debts, 200);
    }

    public function restoreClient($id_clients) 
    {
        $input  = Request::all();
        // $validator = Validator::make($input, ['reason' => 'required']);

        /* if ($validator->fails()) {
            return Main::response(false, 'Bad request', ['errors' => $validator->errors()], 400);
        } */

        $client = Client::where('id_clients', $id_clients)->first();
        if (!$client) {
            return Main::response(false, null, null, 404);
        }
        $bad_debts = BadDebt::where('id_clients', $client->id_clients)
            ->whereNull('id_charges')->get();

        if(count($bad_debts) == 0) {
            return Main::response(false, 'Not Found', null, 200);
        }

        $debts = []; 
        foreach ($bad_debts as $bad_debt) {
            $debt = new Debt;
            $debt->id_clients = $bad_debt->id_clients;
            $debt->id_charges = $bad_debt->id_charges;
            $debt->amount = $bad_debt->amount;
            $debt->collection_fees = $bad_debt->collection_fees;
            $debt->moratory_fees = $bad_debt->moratory_fees;
            $debt->next_try_to_charge = $bad_debt->next_try_to_charge;
           // $restDebt->reason = $input['reason'];
          //  $user = Auth::user();
            //$debt->user_id = $user->id;
            $debt->save();
            $bad_debt->delete();
            $debts[] = $debt;
        }
        $client->bad_debt = false;
        $client->save();
        
        return Main::response(true, 'OK', $debts, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
