<?php

namespace App\Http\Controllers;

use App\Debt;
use App\Contract;
use App\Client;
use App\MySQLClientRepository;
use App\LevelService;
use App\Traits\DebtTrait;
use Request;
use Conekta;
use Conekta_Charge;
use Conekta_Customer;
use Conekta_Plan;
use Conekta_ProcessingError;
use Conekta_ParameterValidationError;

class DebtsController extends Main
{
    use DebtTrait;

    /**
     * index
     * Devuelve todas las deudas (\App\Debt) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Debt
     * 
     * @return response OK|Internal Server Error
     */
    public function index() {

        try {

            $debts = Debt::query();

            foreach(Request::query() as $name => $value) {

                switch($name) {
                    case 'status':
                        if($value == 'unpaid')
                            $debts = $debts->whereNull('id_charges');
                        else
                            $debts = $debts->whereNotNull('id_charges');
                    break;
                    default:
                        $debts = $debts->where($name, $value);
                    break;
                }

            }

            return Main::response(true, 'OK', $debts->get(), 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    /**
     * show
     * Muestra una deuda (\App\Debt) por medio del ID
     *
     * @\App\Debt
     * 
     * @param  int      $id ID de la deuda
     * @return response     OK|Not Found(404)
     */
    public function show($id) {

        if($debt = Debt::find($id)) {

            return Main::response(true, 'Ok', $debt, 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * charge
     * Intenta realizar un cobro via CONEKTA a un cliente(\App\Client).
     * El cliente se busca mediante el id_clients de la deuda.
     * Si se logra realizar el cobro, se le reduce el monto cobrado a la deuda y se le asigna a la deuda el 
     *   ID de transaccion(CONEKTA) del cobro.
     *
     * @Conekta
     * @Conekta_Customer
     * @Conekta_Charge
     * @\App\Client
     * @\App\Debt
     * 
     * @param  int      $id ID de la deuda
     * @return response     OK|Payment Required|Not Found|Internal Server Error
     */
    public function charge($id)
    {
        // NOE si algo falla, elimina estas lineas
        $now = \Carbon\Carbon::now();
        $endOfmonth = \Carbon\Carbon::now()->endOfMonth();

        if ($endOfmonth->isToday() && $now->format('H') > 17) {
            return Main::response(false, 'Forbidden', 'You cant charge a client on the last day of the month after 5pm', 403);
        }
        // HASTA aqui

        $debt = $this->chargeDebt($id);
        switch ($debt) {
            case '403':
                return Main::response(false, 'Forbidden', 'Debt has been paid', 403);
                break;
            
            case '200':
                $response = Debt::find($id);
                return Main::response(true, 'Ok', $response, 200);
                break;
            
            case '402':
                return Main::response(false, 'Payment Required',  ['errors' => ['charge' => ['No fue posible realizar el cargo']]], 402);
                break;
            
            case '404':
                return Main::response(false, 'Not Found', null, 404);
                break;
            
            case '500':
            default:
                return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
                break;
        }

    }

}
