<?php

namespace App\Http\Controllers;

use App\Client;
use Request;

class FinanceClientsController extends Main {

    /**
     * index
     * Devuelve todas los clientes (\App\Client) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Client
     * 
     * @return response Ok|Internal Server Error
     */
    public function index() {

        try {

            $clients = Client::query();
            $clients->where('bad_debt', 0);

            foreach(Request::query() as $name => $value) {

                $clients = $clients->where($name, $value);

            }

            return Main::response(true, 'OK', $clients->get(), 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

}
