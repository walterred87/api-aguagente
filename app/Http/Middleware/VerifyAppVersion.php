<?php

namespace App\Http\Middleware;

use Closure;

class VerifyAppVersion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $appVersion     = $request->input('appVersion');
        $isMovil        = $request->input('isMovilApp');
        $currentVersion = ['2.2.2', '2.2.3'];

        $tokenGet = $request->path();
        if ($isMovil && $tokenGet != 'auth/token' && !in_array($appVersion, $currentVersion)) {
            return response('OldVersionApp', 406);
        }
        return $next($request);
    }
}
