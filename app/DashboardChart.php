<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardChart extends Model
{
    protected $fillable = ['type'];
    const CLICK_VIDEO_SHARE = 1;
    const SHARE_FACEBOOK = 2;
    const SHARE_WHATS_APP = 3;
    const SHARE_OTHER = 4;
}
