<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model {

	protected $table = 'withdrawals';
	protected $primaryKey = 'id_withdrawals';

}

