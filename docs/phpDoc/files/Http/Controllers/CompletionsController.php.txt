<?php

namespace App\Http\Controllers;

use App\Completion;
use App\Http\Controllers\Main;
use App\Assignation;
use App\ErrorCode;
use App\Client;
use App\Ticket;
use App\User;
use Request;
use Validator;
use Mail;

class CompletionsController extends Main {

    /**
     * index
     * Los Completions son los tickets ya finalizados.
     * Devuelve todas las completion (\App\Completion) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Assignation
     * @\App\Completion
     * 
     * @return response OK|Internal Server Error
     */
    public function index() {

        try {

            $completions = Completion::query();

            foreach(Request::query() as $name => $value) {
                
                 switch($name) {

                    case 'id_tickets':

                        $completions = $completions->whereIn(
                            'id_assignations',
                            array_map(
                                function($element) { return $element['id_assignations']; }, 
                                Assignation::where($name, $value)
                                    ->select('id_assignations')
                                    ->get()
                                    ->toArray()
                            )
                        );

                    break;
                    
                    default:

                        $completions = $completions->where($name, $value);

                    break;

                }

            }

            $completions = $completions->get();

            foreach($completions as &$completion) {

                $completion = $this->resolveRelations($completion);
            
            }

            return Main::response(true, 'OK', $completions, 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }
        
    }

    /**
     * show
     * Muestra un completion (\App\Completion) por medio del ID
     * 
     * @\App\Completion
     * 
     * @param  int      $id ID de completion
     * @return response     OK|Not Found
     */
    public function show($id) {

        if($completion = Completion::find($id)) {

            return Main::response(true, 'OK', $this->resolveRelations($completion), 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * resolveRelations
     * Busca una completion (\App\Completion) y asigna todas sus:
     *     - Asignaciones (\App\Assignation)
     *     - Errores (\App\ErrorCode)
     *     - Fotos
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Assignation
     * @\App\Completion
     * @\App\ErrorCode
     * 
     * @param  Completion $completion Objeto Eloquent Category
     * @return Completion             Objeto Eloquent Category
     */
    private function resolveRelations(Completion $completion) {

        $completion->assignation = Assignation::find($completion->id_assignations);
        $completion->error_code = ErrorCode::find($completion->id_error_codes);

        $location = "documents/tickets/" . $completion->assignation->id_tickets;
        $photos = glob("$location/*");
        foreach($photos as &$photo)
            $photo = Request::root() . "/" . $photo;

        $completion->photos = $photos;

        return $completion;

    }

}

